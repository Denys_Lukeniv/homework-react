import React from 'react';
import Product from "../../components/Product/Product";
// import './ProductList.scss'
import PropTypes from 'prop-types';


const ProductList = ({products, openModal, closeModal, addToCart, addToFavorite}) => {

    const productCards = products.map(product => <Product key={product.code} product={product} openModal={openModal}
                                                          closeModal={closeModal} addToCart={addToCart}
                                                          addToFavorite={addToFavorite}
                                                          isFavorite={product.isFavorite}/>)
    return (
        <div className='product-list'>
            {productCards}
        </div>
    );
}
ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        image: PropTypes.node.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.array.isRequired
    })).isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorite: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    closeModal: PropTypes.func.isRequired
}
export default ProductList;
