import React from 'react';
import Product from '../../components/Product/Product';

const Favorites = ({openModal, closeModal, addToCart, addToFavorite}) => {
    const favorites = JSON.parse(localStorage.getItem('favorites'))
    if (favorites) {
        const productCards = favorites.map(product => <Product key={product.code} product={product} openModal={openModal}
                                                          closeModal={closeModal} addToCart={addToCart}
                                                          addToFavorite={addToFavorite}
                                                          addToFavoriteBool={true}
                                                          isFavorite={product.isFavorite}/>)
        return (
            <div className='product-list'>
                {productCards}
            </div>
        );
    } else {
        return (
            <div className='container'>
                <h1 className='title'>No items has been added...</h1>
            </div>
        )
    }
};

export default Favorites;
