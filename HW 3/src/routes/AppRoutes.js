import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import ProductList from "../pages/ProductList/ProductList";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";

const AppRoutes = ({ products, openModal, closeModal, addToCart, addToFavorite, deleteFromCart }) => {

    return (
        <Switch>
            <Redirect exact from='/' to='/products' />
                <Route exact path='/products'><ProductList products={products}
                    addToCart={addToCart}
                    addToFavorite={addToFavorite}
                    openModal={openModal}
                    closeModal={closeModal} /></Route>
                <Route exact path='/cart'><Cart products={products}
                    addToCart={addToCart}
                    addToFavorite={addToFavorite}
                    deleteFromCart={deleteFromCart}
                    openModal={openModal}
                    closeModal={closeModal} /></Route>
                <Route exact path='/favorites'><Favorites products={products}
                                                      addToCart={addToCart}
                                                      addToFavorite={addToFavorite}
                                                      openModal={openModal}
                                                    closeModal={closeModal}/></Route>
        </Switch>
    );
};

export default AppRoutes;
