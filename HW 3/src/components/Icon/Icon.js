import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as allIcons from "../../icons/index";

class Icon extends Component {
    render() {
        const { type, color, className, ...rest} = this.props;

        const Icon = allIcons[type];

        if (!Icon) {
            return null
        }

        return (
            <div className={className} {...rest}>
                {Icon(color)}
            </div>
        );
    }
}

Icon.defaultProps = {
    color: '#000',
    className: 'svg-wrapper',
};

Icon.propTypes = {
    type: PropTypes.string.isRequired,
    color: PropTypes.string,
    className: PropTypes.string
};

export default Icon;