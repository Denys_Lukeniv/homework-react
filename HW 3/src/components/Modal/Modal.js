import React, { PureComponent } from 'react';
import styles from './Modal.module.scss'
import PropTypes from "prop-types";

class Modal extends PureComponent {


    render() {
        const { header, closeButton, text, actions, background, background_content, closeModal } = this.props;

        return (
            <>

                <div className={styles.modal} onClick={e => (e.currentTarget === e.target) && closeModal}>
                    <div onClick={this.deleteModalByBg} className={styles.modal__background}  >
                        <div style={{ backgroundColor: background }} className={styles.modal__content}>
                            <div style={{ backgroundColor: background_content }} className={styles.modal__content__header}>
                                <p className={styles.modal__content__header__title}>{header}</p>
                                {closeButton && <button className={styles.modal__content__header__close} onClick={this.closeModal}>✕</button>}
                            </div>
                            <p className={styles.modal__content__text}>{text}</p>
                            <div className={styles.modal__content__footer}>
                                {actions}
                            </div>
                        </div>
                    </div>

                </div>

            </>

        );
    }
}


Modal.propTypes ={
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.node.isRequired,
    background: PropTypes.string,
    background_content: PropTypes.string,
    closeModal: PropTypes.bool,
}

Modal.defaultProps ={
    closeButton: false,
    background: '',
    background_content: '',
    closeModal: false,
}



export default Modal