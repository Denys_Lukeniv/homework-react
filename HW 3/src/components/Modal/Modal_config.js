import Modal from "./Modal";
import Button from "../Button/Button";
import { Component } from "react";
import PropTypes from 'prop-types';



export const ModalAddToCart = ({closeModal, addToCart, product}) => {
    return (
        <Modal
            header='Do you want to add this product to your cart?'
            text={'You can change your desicion in cart'}
            closeButton={false}
            modalType='modal__container--success'
            closeModal={closeModal}
            desc=''
            actions={<div>
                <Button classes='btn btn__control' text='Yes'
                        handleClick={() => addToCart(product)}/>
                <Button classes='btn btn__control' text='No'
                        handleClick={closeModal}/>
            </div>}
        />
    );
}
export const ModalDeleteFromCart = ({closeModal, deleteFromCart, code}) => {
    return (
        <Modal
            header='Do you want delete this product from your cart?'
            closeButton={false}
            modalType='modal__container--success'
            closeModal={closeModal}
            desc=''
            actions={<div>
                <Button classes='btn btn__control' text='Yes'
                        handleClick={() => deleteFromCart(code)}/>
                <Button classes='btn btn__control' text='No'
                        handleClick={closeModal}/>
            </div>}
        />
    );
}
ModalAddToCart.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired
}
ModalDeleteFromCart.propTypes = {
    closeModal: PropTypes.func.isRequired,
    deleteFromCart: PropTypes.func.isRequired,
    code: PropTypes.number.isRequired
}
