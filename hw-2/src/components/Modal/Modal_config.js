import Modal from "./Modal";
import Button from "../Button/Button";
import { Component } from "react";
import PropTypes from 'prop-types';



export class ModalAddToCart extends Component {
    render() {
        
        const { closeModal, addToCart } = this.props
        return (


            <Modal
                closeButton={false}
                header={'Do you want to add this product to your cart?'}
                modalType='modal__container--success'
                text={'You can change your desicion in cart'}
                actions={
                    <>
                        <Button   text='Yes'
                            handleClick={addToCart} />
                        <Button  text='No'
                            handleClick={closeModal} />
                    </>
                }
            />
        );
    }
}

ModalAddToCart.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired
}
