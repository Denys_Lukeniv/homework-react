import React, { Component } from 'react'
import './App.scss'
import Button from './components/Button/Button'
import styles from '../src/components/Modal/Modal.module.scss';
import Modal from './components/Modal/Modal'


class App extends Component {
	state = {
		closeButton: false,
	}




	render() {
		const { isFirstModal, isSecondModal } = this.state;

		const firstActions = <>
			<button style={{ backgroundColor: '#b3382c' }} className={styles.content__footer__button} onClick={this.closeFirstModal}>Ok</button>
			<button style={{ backgroundColor: '#b3382c' }} className={styles.content__footer__button} onClick={this.closeFirstModal} >Cancel</button>
		</>



		return (
			<>
				{
					isFirstModal &&
					<Modal
						closeButton={true}
						header={'Do you want to delete this file?'}
						text={'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'}
						actions={
							<>
								<button style={{ backgroundColor: '#b3382c' }} className={styles.content__footer__button} onClick={this.closeFirstModal}>Ok</button>
								<button style={{ backgroundColor: '#b3382c' }} className={styles.content__footer__button} onClick={this.closeFirstModal} >Cancel</button>
							</>
						}
					/>
				}

				{
					isSecondModal &&
					<Modal
						background={'#5bd1d7'}
						background_content={"#348498"}
						header={'Do you want to add this to your cart?'}
						text={'Select an option!'}
						actions={
							<>
								<button style={{ backgroundColor: '#b3377c' }} className={styles.content__footer__button} onClick={this.closeSecondModal}>Yes</button>
								<button style={{ backgroundColor: '#b3377c' }} className={styles.content__footer__button} onClick={this.closeSecondModal} >No</button>
							</>
						}
					/>
				}



				<div className='App'>
					{/* <Button backgroundColor={'#5bd1d7'} handleClick={this.openFirstModal} >Open first modal</Button>
					<Button backgroundColor={'#348498'} handleClick={this.openSecondModal} >Open second modal</Button> */}

					<Modal
						backgroundBtn={'#348498'}
						children={'Open first modal'}
						closeButton={true}
						header={'Do you want to delete this file?'}
						text={'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'}
						actions={
							<>
								<button style={{ backgroundColor: '#b3382c' }} className={styles.content__footer__button} onClick={this.clos}>Ok</button>
								<button style={{ backgroundColor: '#b3382c' }} className={styles.content__footer__button} onClick={this.closeFirstModal} >Cancel</button>
							</>
						}
					/>

					<Modal
					backgroundBtn={'#5bd1d7'}
						children={'Open second modal'}
						background={'#5bd1d7'}
						background_content={"#348498"}
						header={'Do you want to add this to your cart?'}
						text={'Select an option!'}
						actions={
							<>
								<button style={{ backgroundColor: '#b3377c' }} className={styles.content__footer__button} onClick={this.closeSecondModal}>Yes</button>
								<button style={{ backgroundColor: '#b3377c' }} className={styles.content__footer__button} onClick={this.closeSecondModal} >No</button>
							</>
						}
					/>

				</div>



			</>
		)
	}
}

export default App
