import React, { Children, PureComponent } from "react";
import styles from './Modal.module.scss';
import Button from "../Button/Button";

class Modal extends PureComponent {

    state = {
        isOpenModal: false
    }
    openModal = () => {
        { this.setState({ isOpenModal: true }) }
    }
    closeModal = () => {
        { this.setState({ isOpenModal: false }) }
    }
    deleteModalByBg = (e) => {
        if (e.target.classList.contains(styles.background)) {
            this.closeModal();
        }
    }

    render() {
        const { header, closeButton, text, actions,backgroundBtn, background, background_content, children } = this.props;
        const { isOpenModal } = this.state

        return (
            <>
                <Button backgroundColor={backgroundBtn} handleClick={this.openModal} >{children}</Button>
                {isOpenModal &&
                    <div className={styles.modal}>
                        <div onClick={this.deleteModalByBg} className={styles.background} ></div>
                        <div style={{ backgroundColor: background }} className={styles.content}>
                            <div style={{ backgroundColor: background_content }} className={styles.content__header}>
                                <p className={styles.content__header__title}>{header}</p>
                                {closeButton && <button className={styles.content__header__close} onClick={this.closeModal}>✕</button>}
                            </div>
                            <p className={styles.content__text}>{text}</p>
                            <div onClick={this.closeModal} className={styles.content__footer}>
                                {actions}
                            </div>

                        </div>
                    </div>

                }
            </>

        );
    }
}





export default Modal